<?php
require_once __DIR__ . '/vendor/autoload.php';

use Collage\App\Rest;
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

$ctrl = new Rest\Controller();
echo $ctrl->dispatch($_SERVER);
