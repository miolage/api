<?php
namespace Collage\App\Io;

use \PDO;

class MySql {
	private static $db;

	public static function getDB() {
		if(!self::$db) {
			self::$db = new PDO( 
				'mysql:host='.getenv('DB_HOST').';dbname='.getenv('DB_DATABASE').';port='.getenv('DB_PORT'), 
				getenv('DB_USER'), 
				getenv('DB_PASSWORD'), 
				array( 
					PDO::ATTR_PERSISTENT => true, 
					PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION ,
					PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
				) 
			);
		}
		return self::$db;
    }
}
