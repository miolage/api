<?php

namespace Collage\App\Models;

/**
 *
 */
class Collage extends Model {

	/**
	 * @var int
	 */
	public $id = 0;

	/**
	 * @var int
	 */
	public $template_id = 0;

	/**
	 * @var string
	 */
	public $code = '';

	/**
	 * @var string
	 */
	public $title = '';

	/**
	 * @var string
	 */
	public $desc = '';
}
