<?php

namespace Collage\App\Models;

use Collage\App\Io;
use \PDO;

class Model {

	public function __construct(array $data = []) {
		// TODO: only allow to set defines variables
		foreach ($data as $key => $val) {
			$this->$key = $val;
		}
	}

	public static function findAllBy($column, $value, array $fields = [], array $filters = [], $limit = 0, array $order_by = array()) {
		$allowedFields = array_keys(get_class_vars(static::class));
		if ($fields) {
			$fields = array_intersect($fields, $allowedFields);
		} else {
			$fields = $allowedFields;
		}

		$table = strtolower(basename(str_replace('\\', '/', static::class)));
		$sql = 'SELECT `'.implode('`,`', $fields).'` FROM ' . $table . ' WHERE ' . $column . ' = :value';
		$params = [
			':value' => $value
		];
		$and = [];
		$i = 0;
		foreach ($filters as $filter) {
			$and[] = basename($filter['name']) . ' ' . $filter['op'] . ' :f' . $i;
			$params[':f' . $i] = $filter['val'];
			$i++;
		}
		if ($i) {
			$sql .= ' AND (' . implode(' AND ', $and) . ')';
		}
		if ($order_by) {
			$sql .= ' ORDER BY';
			foreach ($order_by as $row) {
				$sql .= sprintf(' %s %s,', basename($row['field']), basename($row['direction']));
			}
			$sql = rtrim($sql, ',');
		}
		if ($limit) {
			$sql .= ' LIMIT ' . intval($limit);
		}

		$stmt = Io\Mysql::getDB()->prepare($sql);
		$stmt->setFetchMode(PDO::FETCH_CLASS, static::class);
		$stmt->execute($params);
		return $stmt->fetchAll();
	}

	public static function findBy($column, $value) {
		$table = strtolower(basename(str_replace('\\', '/', static::class)));
		$sql = 'SELECT * FROM ' . $table . ' WHERE ' . $column . ' = :value LIMIT 1';
		$stmt = Io\Mysql::getDB()->prepare($sql);
		$stmt->setFetchMode(PDO::FETCH_CLASS, static::class);
		$stmt->execute([
			':value' => $value
		]);
		return $stmt->fetch();
	}

	public function save() {
		$table = strtolower(basename(str_replace('\\', '/', static::class)));
		$data = get_object_vars($this);

		// UPDATE
		if ($this->id) {
			$sql = 'UPDATE ' . $table . ' SET ';

			// change updated_at
			if (isset($data['updated_at'])) {
				$data['updated_at'] = date('Y-m-d H:i:s');
			}

			foreach ($data as $key => $val) {
				if ($key == 'id') {
					continue;
				}
				$sql .= $key . '=:' . $key . ',';
			}
			$sql = rtrim($sql, ',') . ' WHERE id = :id';
			$stmt = Io\Mysql::getDB()->prepare($sql);
			return $stmt->execute($data);
		} // INSERT
		else {
			if (isset($data['name']) && empty($data['name'])) {
				$data['name'] = (new \Nubs\RandomNameGenerator\Alliteration())->getName();
				$this->name = $data['name'];
			}

			$str_keys = implode(',', array_keys($data));
			$str_vals = ':' . str_replace(',', ',:', $str_keys);

			$sql = 'INSERT INTO ' . $table . ' (' . $str_keys . ') 
				VALUES (' . $str_vals . ')';
			$stmt = Io\Mysql::getDB()->prepare($sql);
			return $stmt->execute($data);
		}
	}
}
