<?php
namespace Collage\App\Rest;

use Collage\App\Models;


/**
 */
class Controller {
	protected $result;

	public function __construct() {
		$this->result = new Result();
	}

	/**
	 * handle HTTP POST with content type application/x-www-form-urlencoded
	 * used for single inserts
	 */
	protected function postForm($class, array $data) {
		if (!empty($data['id'])) {
			$model = $class::findBy('id', $data['id']);
		} else {
			$model = new $class();
		}
		foreach($data as $key => $val) {
			$model->$key = $val;
		}
		$model->save();
		$this->result->add($model);
	}

	/**
	 * handle HTTP GET
	 */
	protected function get($class, array $filters, array $fields, $limit, array $order_by) {
		foreach ($class::findAllBy("1", 1, $fields, $filters, $limit, $order_by) as $model) {
			$this->result->add($model);
		}
	}

	protected function sendHeader($str) {
		header($str);
	}

	/**
	 * check https://jsonapi.org/format/#fetching-sparse-fieldsets
	 */
	public function dispatch(array $server) {
		$this->sendHeader('Content-Type: application/json');
		$this->sendHeader('Access-Control-Allow-Origin: *');
		$this->sendHeader('Access-Control-Allow-Headers: Authorization');

		if (preg_match('/\/([a-z]+)\/?([0-9]*)/', $server['REQUEST_URI'], $match)) {
			$route = $match[1];
		} else {
			throw new \Exception('invalid request');
		}

		switch ($route) {
			case 'picture':
				$class = Models\Picture::class;
				break;
			case 'collage':
				$class = Models\Collage::class;
				break;
			default:
				throw new \Exception('unknown model: '.$route);
		}

		$method = strtoupper(basename($server['REQUEST_METHOD']));
		switch($method) {
			case 'POST':
				switch ($server["CONTENT_TYPE"]) {
					case "application/json":
					case "application/json; charset=UTF-8":
						$data = json_decode(file_get_contents('php://input'), true);
						break;
					default:
						$data = $_POST;
						break;
				}
				if (empty($data["collage_id"])) {
					die("empty collage not allowed");
				}
				$this->postForm($class, $data);
				break;
			case 'GET':
				$q = isset($_GET['q']) ? json_decode($_GET['q'], true) : [];
				$filters = isset($q['filters']) ? $q['filters'] : [];
				$fields = isset($q['fields']) ? explode(',', $q['fields']) : [];
				$limit = isset($q['limit']) ? intval($q['limit']) : 0;
				$order_by = isset($q['order_by']) ? $q['order_by'] : [];
				if ($match[2] !== "") {
					$filters[] = [
						"name" => "id",
						"op" => "=",
						"val" => $match[2]
					];
				}
				$this->get($class, $filters, $fields, $limit, $order_by);
				break;
			default:
				throw new \Exception('unknown request method: ' . $method);
		}

		return $this->result->getJson();
	}
}