<?php
namespace Collage\App\Rest;

use Collage\App\Model;

/**
 */
class Result {

	/**
	 * @var Model\Model[]
	 */
	private $objects = array();

	/**
	 * @param Model\Model $object
	 */
	public function add($object) {
		$this->objects[] = $object;
	}

	/**
	 * @return string
	 */
	public function getJson() {
		$json = [];
		$json['num_results'] = count($this->objects);
		$json['objects'] = array_map("get_object_vars", $this->objects);
		return json_encode($json, JSON_NUMERIC_CHECK);
	}
}